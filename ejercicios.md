# Ejercicios


## ejercicio1.php

- Crea un fichero PHP donde te presesntes como alumno.
- Tu información (nombre, apellidos, edad, ...) debe estar guardada en variables.
- Hazlo usando echo y print
- Usa el entrecomillado simple y doble para ver las distintas posibilidades.
- Usa etiquetas html para mejorar la presentación.


## ejercicio2.php


- Vamos a calcular el precio con IVA de un producto.
- Carga los datos en variables (precioUnidad, cantidad, iva, precioSinIva, precioConIva, ...)
- Realiza los cálculos pertinentes y muestra detalladamente cada valor inicial y calculado.


## ejercicio3.php


- Crea un array ordenado con los días de la semana.
- Muestra el contenido del array.
- Hazlo de distintas maneras: declaración del array en una sola sentencia, añadiendo elementos uno por uno, ....
- Usa `<ul>...</ul>` o `<table>...</table>`


## ejercicio4.php


- Crea un array asociativo que contenga el quinteto inicial de un equipo de baloncesto.
- Muestra su contenido con un bucle foreach.
- Intenta distintas varianttes, accediendo o no a la clave -> valor.



## Ejercicio5.php

- Crea dos ficheros ejercicio5.html y ejercicio5.php.
- El html debe dar la información de registro de un usuario.
- Nombre
- Apellidos
- Edad
- Aficiones (usar varios checkbox)
- Sexo (usar _radio_)
- Deporte favorito (usar un select).
- El fichero php debe mostrar por pantalla la información por pantalla usando una <ul>.



## Ejercicio6.php


- Modifica el ejercicio anterior para que las aficiones se reciban como un array _hobbies[]_


## Ejercicio7.php
- En el ejercicio 7 vammos a usar un sólo fichero _ejercicio7.php_
- El formulario va a enviar los datos a si mismo.
- Vamos a formar una lista de jugadores de futbol, cada vez que enviemos guardaremos un nuevo elemento en la lista.
- PISTA: usa control(es) de tipo _hidden_.
## Revisiar lo aprendido y refactorizar.
- Aunque aún no hablemos de MVC (Modelo Vista Controlador). Vamos a separar la presentación de la lógica del programa.
- Vamos a separar el código en funciones.
- En php podemos incluir el contenido de un fichero en otro usando:
- require: se incluye un fichero, si no se encuentra de error.
- include: se incluye un fichero, si no se encuentra sigue adelante.
- Existen require_once e include_once. Veremos su uso.


## Ejercicio8.php
- Parte del ejercicio7.
- Lleva la construcción de html a un fichero ejercicio8vista.php.
- La lógica déjala en ejercicio8.php
- Refactoriza la lógica: usa funciones.


## ejercicio9.php

### Parte 1

- Crea una carpeta: ejercicio9
- TODAS LAS PETICIONES LAS DEBE ATENDER index.php.
- index.php muestra un formulario para rellenar el nombre.
- Al enviar el formulario, debemos tomar el nombre enviado y guardarlo en una cookie.
- El formulario debe recibirlo el propio index. Investiga y usa `_$PHP_SERVER['PHP_SELF']`.
- Separa vista y funcionalidad.

### Parte 2.

- Modifica la vista para que si ya existe la cookie el encabezado muestre la información del usuario.

### Parte 3.

- Añade a la vista un enlace de logout.


## Ejercicio 10

- Modifica el ejercicio anterior para conseguir la misma funcionalidad usando POO.
- Crea una clasee App con dos métodos, index y home.
- El método index muestra el formulario de login. Si existe cookie el nombre de usuario debe aparece completado.
- El método home recoge los datos del formulario y guarda la cookie. Además da la bienvenida si existe nombre de usuario y si no pregunta _¿Quién eres?_

## Ejerciio 11

Ejercicio de sesiones y POO. Se trata de crear una lista de deseos Usaremos la clase App con los siguietens métodos:

- **login** método que muestra formulario de entrada.
- **auth** método que toma el nombre de usuario tras el login. Tras hacer esto reenvía a **home**.
- **home** método que muestra la lista de deseos. Además muestra un formulario de nuevos deseos. El formulario envía al método **new**
- **new** toma el nuevo deseo y lo incluye en la lista.
- **delete** borra un deseo de la lista. Debe recibir el indice del deseo.
- **empty** vacía la lista de deseos.
- **close** Cierra sesión. Olvida deseos y usuario.
