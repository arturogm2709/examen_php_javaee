# Ejercicios de introduccion PHP

## Empezar

- Haz un fork de este repositorio
- Clona en tu equipo el repositorio que acabas de crear.

## Configuración de apache

- Copia el fichero de configuración suministrado en /etc/apache2/sites-available/
- Para copiarlo usar sudo:

```
sudo cp ejerciciosphp.conf /etc/apache2/sites-available/
```

- Activa el nuevo sitio web: `sudo a2ensite ejerciciosphp.conf`

- Reinicia apache: `sudo service apache2 restart`


## Resolución de nombres


 - Abre con sudo el fichero /etc/hosts y añade la línea:

```
127.0.0.1   ejerciciosphp.local
```

## Comprobación

- Abre en tu navegador `http://ejerciciosphp.local`

